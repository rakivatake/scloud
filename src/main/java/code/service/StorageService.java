package code.service;

import code.dao.exceptions.FailedToCreateUserException;
import code.dao.model.User;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {

    void save(MultipartFile multipartFile, User user);

    Resource loadAsResource(String filename, Long userId);

    void delete(String filename, User user) throws FailedToCreateUserException;
}
