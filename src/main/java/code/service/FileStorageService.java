package code.service;

import code.dao.exceptions.FailedToCreateUserException;
import code.dao.model.User;
import code.dao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Service
public class FileStorageService implements StorageService{

    private final UserService userService;

    @Autowired
    public FileStorageService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void save(MultipartFile file, User user) {
        String uploadDir = user.getUserDir();
        File filePath = new File(uploadDir + "/" + file.getOriginalFilename());

        if (filePath.exists()){
            filePath.delete();
        }

        try {

            filePath.createNewFile();

            FileOutputStream outputStream = new FileOutputStream(filePath);

            outputStream.write(file.getBytes());

            user.getFilenames().add(file.getOriginalFilename());

            userService.save(user);
        }catch (Exception e){
            throw new RuntimeException("Failed to save file");
        }
    }

    @Override
    public Resource loadAsResource(String filename, Long userId) {
        return userService.findById(userId)
                .map(user -> {
                    String userDir = user.getUserDir();

                    if (!user.getFilenames().contains(filename)){
                        throw new RuntimeException(String.format("User doesn't have file [%s]", filename));
                    }

                    File file = new File(userDir + "/" + filename);

                    try {
                        return new InputStreamResource(new FileInputStream(file));
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException("Failed to find file", e);
                    }
                }).orElseThrow(() -> new RuntimeException(String.format("Failed to load file [%s]", filename)));
    }

    @Override
    public void delete(String filename, User user) throws FailedToCreateUserException {
        String uploadDir = user.getUserDir();
        File filePath = new File(uploadDir + "/" + filename);
        filePath.delete();
        user.getFilenames().remove(filename);
        userService.save(user);
    }
}
