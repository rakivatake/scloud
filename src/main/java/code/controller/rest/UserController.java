package code.controller.rest;

import code.dao.model.User;
import code.dao.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/rest/api")
@Slf4j
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/createUser")
    @ResponseBody
    public ResponseEntity createUser(@RequestBody User user){
        log.info("Starting registration of user [{}]", user.getUsername());

        try {
            userService.createUser(user);
        } catch (Exception e) {
            log.error("Failed to create user [{}]", user.getUsername(), e);
            throw new HttpMessageNotReadableException(e.getMessage());
        }

        log.info("User [{}] created.", user.getUsername());

        return ResponseEntity.status(200).build();
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<User> getUserById(@RequestParam("id") Long id) {
        return userService.findById(id)
                .map(ResponseEntity::ok)
                .orElseThrow(() ->
                        new HttpMessageNotReadableException(String.format("Failed to find user with id [%d]", id)));
    }


    @GetMapping("/getUserFiles")
    @ResponseBody
    public ResponseEntity<Set<String>> getUserFiles(@RequestParam("id") Long id) {
        return userService.findById(id)
                .map(User::getFilenames)
                .map(ResponseEntity::ok)
                .orElseThrow(() ->
                        new HttpMessageNotReadableException(String.format("Failed to find user with id [%d]", id)));
    }

}
