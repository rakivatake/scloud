package code.controller.rest;

import code.dao.model.User;
import code.service.StorageService;
import code.dao.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/rest/api")
public class FileController {

    private final UserService userService;
    private final StorageService storageService;

    @Autowired
    public FileController(UserService userService, StorageService storageService) {
        this.userService = userService;
        this.storageService = storageService;
    }

    @PostMapping(path = "/upload")
    public ResponseEntity uploadFile(@AuthenticationPrincipal User user,
                                     @RequestParam("file") MultipartFile file) {

        if (file == null || !userService.existsById(user.getId())){
            throw new HttpMessageNotReadableException("File can't be null");
        }

        try {
            storageService.save(file, user);
        }catch (Exception e){
            throw new HttpMessageNotReadableException("Failed to save file: " + e.getMessage());
        }

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(path = "/download")
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@RequestParam("userId") Long userId,
                                                 @RequestParam("filename") String filename) {

        if (userId == null || StringUtils.isBlank(filename) || !userService.existsById(userId)){
            throw new HttpMessageNotReadableException("Invalid params.");
        }

        try {
            Resource resource = storageService.loadAsResource(filename, userId);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=\"" + filename + "\"")
                    .body(resource);
        }catch (Exception e){
            throw new HttpMessageNotReadableException("Failed to download file:" + e.getMessage());
        }
    }

    @DeleteMapping
    public ResponseEntity deleteFile(@RequestParam("filename") String filename, @AuthenticationPrincipal User user){

        if (filename.isEmpty()){
            return ResponseEntity.ok().build();
        }

        if (!user.getFilenames().contains(filename)){
            throw new HttpMessageNotReadableException("Failed to delete file.");
        }

        try {
            storageService.delete(filename, user);
        }catch (Exception e){
            throw new HttpMessageNotReadableException("Failed to delete file: " + e.getMessage());
        }

        return ResponseEntity.status(204).build();
    }
}
