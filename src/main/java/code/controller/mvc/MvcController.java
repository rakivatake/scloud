package code.controller.mvc;


import code.dao.model.User;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MvcController {

    @GetMapping({"/","/main"})
    public String getMainPage(@AuthenticationPrincipal User user, Model model){
        model.addAttribute("user", user);
        return "/main";
    }
}
