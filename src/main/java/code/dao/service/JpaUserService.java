package code.dao.service;

import code.dao.entity.UserEntity;
import code.dao.exceptions.FailedToCreateUserException;
import code.dao.exceptions.UserAlreadyExistsException;
import code.dao.model.User;
import code.dao.repository.UserRepository;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;

import javax.validation.ConstraintViolationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

@Service
public class JpaUserService implements UserService{

    private final UserRepository repository;

    private final String UPLOAD_PATH;

    @Autowired
    public JpaUserService(UserRepository repository,@Value("${upload.path}") String upload_path) {
        this.repository = repository;
        UPLOAD_PATH = upload_path;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return Optional.ofNullable(repository.findByUsername(username))
                .map(this::mapFromEntity)
                .orElseThrow(() -> new UsernameNotFoundException("Failed to find user in the database"));
    }

    @Override
    public void createUser(User user) throws FailedToCreateUserException{
        try {
            if (repository.findByUsername(user.getUsername()) != null){
                throw new UserAlreadyExistsException("User with such username already exists");
            }

            user.setUserDir(UPLOAD_PATH + "/" + user.getUsername() + "/");

            File file = new File(user.getUserDir());

            if (!file.exists()){
                file.mkdir();
            }

            user.setFilenames(Collections.emptySet());

            save(user);
        } catch (UserAlreadyExistsException e){
            throw new FailedToCreateUserException(e.getMessage(), e);
        }

    }

    @Override
    public void save(User user) throws FailedToCreateUserException {
        try {

            UserEntity entity = Optional.ofNullable(repository.findByUsername(user.getUsername()))
                    .map(userEntity -> {
                        userEntity.setFilenames(user.getFilenames());
                        userEntity.setUserDir(user.getUserDir());
                        userEntity.setUsername(user.getUsername());
                        userEntity.setPassword(userEntity.getPassword());
                        return userEntity;
                    })
                    .orElse(mapToEntity(user));

            repository.save(entity);
        } catch (TransactionSystemException e) {
            Throwable rootCause = ExceptionUtils.getRootCause(e);
            if (rootCause instanceof ConstraintViolationException){
                throw new IllegalArgumentException("Username and password can't be blank", rootCause);
            }
        } catch (Exception e){
            throw new FailedToCreateUserException(e.getMessage(), e);
        }
    }

    @Override
    public Optional<User> findById(Long id) {
        return repository.findById(id).map(this::mapFromEntity);
    }

    @Override
    public Optional<User> findByUsernameAndPassword(String username, String password) {
        return Optional.ofNullable(repository.findByUsernameAndPassword(username,password)).map(this::mapFromEntity);
    }

    @Override
    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

    private User mapFromEntity(UserEntity entity) {
        return User.builder()
                .id(entity.getId())
                .username(entity.getUsername())
                .password(entity.getPassword())
                .userDir(entity.getUserDir())
                .filenames(new HashSet<>(entity.getFilenames()))
                .build();
    }

    private UserEntity mapToEntity(User model) {
        return UserEntity.builder()
                .username(model.getUsername())
                .password(model.getPassword())
                .userDir(model.getUserDir())
                .filenames(model.getFilenames())
                .build();
    }
}
