package code.dao.service;

import code.dao.exceptions.FailedToCreateUserException;
import code.dao.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {

    void createUser(User user) throws FailedToCreateUserException;

    Optional<User> findById(Long id);

    Optional<User> findByUsernameAndPassword(String username, String password);

    boolean existsById(Long id);

    void save(User user) throws FailedToCreateUserException;
}
