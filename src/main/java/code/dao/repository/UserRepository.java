package code.dao.repository;

import code.dao.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

    UserEntity findByUsername(String username);
    UserEntity findByUsernameAndPassword(String username, String password);
    boolean existsById(Long id);
}
