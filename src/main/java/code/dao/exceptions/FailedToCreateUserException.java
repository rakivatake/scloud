package code.dao.exceptions;

public class FailedToCreateUserException extends Exception{

    public FailedToCreateUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
