package code.dao.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Builder
@Table(name = "usr")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usr_id")
    private Long id;

    @NonNull
    @NotBlank
    @Column(unique = true)
    private String username;

    @NonNull
    @NotBlank
    private String password;

    @NonNull
    @NotBlank
    @Column(unique = true)
    private String userDir;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> filenames;
}
