var App = Marionette.Application.extend({
    region: 'body',

    //Создание корневого представления
    onStart(){
        App.Router = new AppRouter();
        Backbone.history.start();

        const root = new RegistrationRoot({});

        root.render();
        this.showView(root);
    }
});

var AppRouter = Backbone.Router.extend({
});

//Запуск приложения
$(function () {
    new App().start();
});
