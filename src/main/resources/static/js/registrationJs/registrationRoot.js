const RegistrationRoot = Marionette.View.extend({
    tagName: "main",
    template: registrationRootTemplate,

    events: {
        'click #registration':'registrationUser'
    },

    onAttach() {
        $('main').addClass("flex-grow-1 flex-shrink-1 d-flex h-100");
    },

    registrationUser(){
        let re = new RegExp("^ *$");

        let userName = $("#nameInput").val();
        let password = $("#passwordInput").val();

        if(re.test(userName)){
            $("#message").text("login can't be empty");

            $("#nameInput").css("border-color", "red");

            setTimeout(function () {
                $("#message").text("");
                $("#nameInput").css("border-color", "");
            }, 3000);
        } else {
            if(re.test(password)){
                $("#message").text("password can't be empty");

                $("#passwordInput").css("border-color", "red");

                setTimeout(function () {
                    $("#message").text("");
                    $("#passwordInput").css("border-color", "");
                }, 3000);
            } else {
                let url = UrlStorage.getCreateUserUrl();

                let user = {
                    username: userName,
                    password: password
                };

                fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(user)
                }).then(response => {
                    if(response.status === 200){
                        let origin = window.location.origin;
                        let newPathname = "/";

                        window.location = origin + newPathname;
                    } else if(response.status > 399 && response.status < 500){
                        $("#message").text("user with this login already exist");

                        setTimeout(function () {
                            $("#message").text("");
                        }, 3000);
                    } else {
                        $("#message").text("server error");

                        setTimeout(function () {
                            $("#message").text("");
                        }, 3000);
                    }

                }).catch(error => {
                    console.log(error);
                });
            }
        }
        //;
    }
});