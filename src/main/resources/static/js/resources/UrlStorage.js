class UrlStorage {
    static getUserFilesUrl(id) {
        return "rest/api/getUserFiles?id=" + id
    }

    static getUploadFileUrl(){
        return "rest/api/upload"
    }

    static getDownloadFileUrl(id, filename){
        return "rest/api/download?userId=" + id + "&filename=" + filename
    }

    static getCreateUserUrl(){
        return "rest/api/createUser"
    }

    static getDeleteFileUrl(filename){
        return "rest/api/?filename=" + filename
    }
}