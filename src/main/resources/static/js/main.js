var App = Marionette.Application.extend({
    region: '.replaceElem',

    //Создание корневого представления
    onStart(){
        let id = localStorage.getItem("id");
        let name = localStorage.getItem("name");

        let user = new Backbone.Model({
            id: id,
            name: name,
            loaded: false
        });

        App.Router = new AppRouter();
        Backbone.history.start();

        const root = new Root({
            model: user});

        root.render();
        this.showView(root);
    }
});

var AppRouter = Backbone.Router.extend({
});

//Запуск приложения
$(function () {
    new App().start();
});
