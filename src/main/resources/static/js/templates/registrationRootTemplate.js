var registrationRootTemplate = _.template('<div class="container-fluid px-0 h-100">\n' +
    '        <nav class="navbar navbar-dark fixed-top bg-primary">\n' +
    '            <h4 class="whiteFont">sCloud</h4>\n' +
    '        </nav>\n' +
    '        <div class="row justify-content-center align-items-center h-100">\n' +
    '            <div class="col-xl-5 col-lg-4 col-md-2 col-sm-1 col-0"></div>\n' +
    '            <div class="col-xl-2 col-lg-4 col-md-6 col-sm-10 col-12 uploadPadding">\n' +
    '                <h4 class="loginHeader">Registration</h4>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\n' +
    '                        <label> User Name :  </label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row loginRow">\n' +
    '                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\n' +
    '                        <input id="nameInput" class="form-control" type="text" name="username"/>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\n' +
    '                        <label> Password:  </label>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row loginRow">\n' +
    '                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\n' +
    '                        <input id="passwordInput" class="form-control" type="password" name="password"/>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row loginRow">\n' +
    '                    <div class="col-12">\n' +
    '                        <div><button id="registration" class="btn btn-outline-primary btn-block">Create user</button></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div id="message" class="redFont"></div>\n' +
    '            </div>\n' +
    '            <div class="col-xl-5 col-lg-4 col-md-2 col-sm-1 col-0"></div>\n' +
    '        </div>\n' +
    '    </div>');