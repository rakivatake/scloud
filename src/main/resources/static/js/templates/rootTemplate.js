var rootTemplate = _.template('<div style="margin-top: 5rem" class="row justify-content-center align-items-center no-gutters">' +
    '<div class="col-12 align-self-center">\n' +
    '    <form id="uploaderForm" enctype="multipart/form-data" method="POST">\n' +
    '    <div class="input-group loadWidth uploadPadding">\n' +
    '      <div class="custom-file">\n' +
    '        <input type="file" class="custom-file-input" id="uploadInput" aria-describedby="inputGroupFileAddon04">\n' +
    '        <label class="custom-file-label" for="uploadInput">Choose file</label>\n' +
    '      </div>\n' +
    '     <div class="input-group-append">\n' +
    '       <button class="btn btn-outline-secondary" type="button" id="upload">Upload</button>\n' +
    '     </div>\n' +
    '    </div>\n' +
    '    </form></div></div>\n' +
    '<div style="margin-top: 1rem" class="row justify-content-start align-items-start no-gutters h-100">\n' +
    '            <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"></div>\n' +
    '            <div id="bodyRegion" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">\n' +
    '            </div>\n' +
    '</div>');