const Root = Marionette.View.extend({
    template: rootTemplate,
    user: null,
    files: [],

    modelEvents: {
        'change:loaded': 'showFiles'
    },

    regions: {
        'body': '#bodyRegion',
    },

    events: {
        'click #upload': 'uploadFile',
        'change #uploadInput': 'changeFileLabel'
    },

    initialize(options){
        this.user = options.user;
    },

    onRender() {
        this.loadUserFiles();
    },

    loadUserFiles(){
        let model = this.model;
        let files = this.files;
        let url = UrlStorage.getUserFilesUrl(model.get("id"));

        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
            .then(response => {
                files.length = 0;

                response.forEach(function (element) {
                    files.push(element)
                });

                model.set({"loaded" : true})
            }).catch(error => {
        });
    },

    uploadFile(){
        let url = UrlStorage.getUploadFileUrl();
        var input = $("#uploadInput");
        let that = this;

        var data = new FormData();
        data.append('file', input[0].files[0]);

        fetch(url, {
            method: 'POST',
            body: data
        }).then(response => {
            $(".custom-file-label").text("chooseFile");
            document.getElementById('uploadInput').innerHTML = document.getElementById('uploadInput').innerHTML;
            that.loadUserFiles();
        }).catch(error => {
            console.log(error);
        });
    },

    showFiles(){
        let model = this.model;
        let files = this.files;

        if(model.get("loaded")){
            if(this.bodyView !== undefined){
                this.bodyView.destroy()
            }

            let bodyView = new FilesCollectionView();
            this.bodyView = bodyView;

            let mainRegion = this.getRegion('body');

            mainRegion.show(bodyView);

            if(files.length === 0){
                let emptyModel = new Backbone.Model({
                    empty: true
                });

                bodyView.collection.push(emptyModel)
            } else {
                files.forEach(function (elem) {
                    let fileModel = new Backbone.Model({
                        empty: false,
                        nameOfFile: elem,
                        userId: model.get("id")
                    });

                    bodyView.collection.push(fileModel)
                });
            }

            model.set("loaded", false);
        }
    },

    changeFileLabel(){
        let file = $("#uploadInput")[0].files[0];

        if(file !== undefined){
            let fileName = file.name;


            if(fileName.length > 12){
                fileName = fileName.slice(0, 12) + "..."
            }

            $(".custom-file-label").text(fileName);
        } else {
            $(".custom-file-label").text("chooseFile");
        }
    }
});