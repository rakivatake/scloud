var FileItemView = Marionette.View.extend({
    tagName: "li",
    template: fileItemTemplate,

    events: {
        'click .loadBut': 'downloadFile',
        'click .deleteBut': 'deleteFile',
    },

    onAttach(){
        this.$(".loadBut").text(this.model.get("nameOfFile"))
    },

    onBeforeDestroy(){
        this.model.destroy();
        this.$el.remove();
    },

    downloadFile(){
        let id = this.model.get("userId");
        let fileName = this.model.get("nameOfFile");

        let url = UrlStorage.getDownloadFileUrl(id, fileName);

        fetch(url, {
            method: 'GET'
        }).then(response => response.blob())
            .then(function(data) {
                let a = document.createElement("a");
                let file = new Blob([data]);
                a.href = URL.createObjectURL(file);
                a.download = fileName;
                a.click();
                $(a).remove();
        }).catch(error => {
            console.log(error);
        });
    },

    deleteFile(){
        let fileName = this.model.get("nameOfFile");

        let url = UrlStorage.getDeleteFileUrl(fileName);

        fetch(url, {
            method: 'DELETE'
        }).then(response => {
            if (response.status === 204) {
                this.destroy();
            }
        }).catch(error => {
            console.log(error);
        });
    }
});