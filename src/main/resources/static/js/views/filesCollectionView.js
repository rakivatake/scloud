var FilesCollectionView = Marionette.CollectionView.extend({
    collection: new Backbone.Collection(),

    childViewContainer: '.collectionBody',
    template: collectionTemplate,

    childView(item) {
        // Choose which view class to render,
        // depending on the properties of the item model
        if (item.get('empty')) {
            return EmptyItemView;
        } else {
            return FileItemView;
        }
    },

    onBeforeDestroy(){
        let that = this;
        let collection = this.children._views;

        while(collection.length > 0){
            that.removeChildView(collection[0]);
        }
    },
});