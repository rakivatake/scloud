var EmptyItemView = Marionette.View.extend({
    template: emptyItemTemplate,

    onBeforeDestroy(){
        this.model.destroy();
        this.$el.remove();
    },
});